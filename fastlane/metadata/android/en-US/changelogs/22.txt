+ ADDED FRENCH AND SPANISH LANGUAGE!!!
+-> Huge thanks to @kernoeb (Telegram) for French translation and @sguinetti (GitLab) for Spanish translation!!

+ added custom post time format support!
+ fixed flickering after changing settings
+ fixed posts not showing after searching from a private profile
+ fixed stories and profile pictures not downloading in user folders even when option was enabled
+ fixed issues with feed, discover and post viewer
+ fixed search suggestions crashes
