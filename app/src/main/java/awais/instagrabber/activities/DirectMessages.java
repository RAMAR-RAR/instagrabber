package awais.instagrabber.activities;

import android.os.Bundle;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.view.ViewCompat;

import awais.instagrabber.R;

public final class DirectMessages extends BaseLanguageActivity {
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final LinearLayout linearLayout = new LinearLayout(this);
        final AppCompatImageView imageView = new AppCompatImageView(this);
        final AppCompatTextView textView = new AppCompatTextView(this);
        final String lol = "Soon, my child";
        imageView.setImageResource(R.drawable.bobs);
        textView.setText(lol);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24);

        ViewCompat.setPaddingRelative(textView, 100, 100, 100, 100);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        final LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
        linearLayout.addView(imageView, layoutParams);
        linearLayout.addView(textView, layoutParams);

        ViewCompat.setPaddingRelative(linearLayout, 100, 100, 100, 100);

        setContentView(linearLayout);
    }
}
