package awais.instagrabber.utils;

public enum DownloadMethod {
    DOWNLOAD_MAIN,
    DOWNLOAD_DISCOVER,
    DOWNLOAD_FEED,
    DOWNLOAD_POST_VIEWER,
    DOWNLOAD_DIRECT
}