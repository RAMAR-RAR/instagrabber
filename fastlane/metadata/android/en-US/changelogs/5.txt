+ added followers / following checker
+ added search view suggestions for usernames & hashtags
+ added sliding profile container
+ fixed batch download permission issue (issue #4)
+ fixed some small screen panning issues
+ fixed search view width
+ fixed update checker