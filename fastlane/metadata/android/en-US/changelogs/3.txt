+ fixed posts merged from different accounts when searched while posts are loading!

+ view stories (only if you're logged in)

+ directly download posts

+ choose between two pfp (profile picture) viewer methods

+ fixed search box not showing up when toolbar is at bottom

+ automatically checks for updates
