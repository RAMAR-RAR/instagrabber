package awais.instagrabber.utils;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;

import java.net.HttpURLConnection;
import java.net.URL;

import awais.instagrabber.BuildConfig;
import awais.instagrabber.interfaces.FetchListener;

public final class UpdateChecker extends AsyncTask<Void, Void, Boolean> {
    private final FetchListener<Boolean> fetchListener;

    public UpdateChecker(final FetchListener<Boolean> fetchListener) {
        this.fetchListener = fetchListener;
    }

    @NonNull
    @Override
    protected Boolean doInBackground(final Void... voids) {
        boolean result = false;
        final String UPDATE_BASE_URL = "https://gitlab.com/AwaisKing/instagrabber/-/releases/v";
        final String versionName = BuildConfig.VERSION_NAME;
        final int index = versionName.indexOf('.');

        try {
            final int verMajor = Integer.parseInt(versionName.substring(0, index));

            // check major version first
            HttpURLConnection conn = (HttpURLConnection) new URL(UPDATE_BASE_URL + (verMajor + 1) + ".0-fdroid").openConnection();
            conn.setUseCaches(false);
            conn.setRequestMethod("HEAD");
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(15000);
            conn.connect();

            int responseCode = conn.getResponseCode();
            if (responseCode == 200 || responseCode == 201) result = true;
            else {
                final int verMinor = Integer.parseInt(versionName.substring(index + 1, versionName.indexOf('-'))) + 1;
                for (int i = verMinor; i < 10; ++i) {
                    final String minorUpdateUrl = UPDATE_BASE_URL + verMajor + '.' + i + "-fdroid";
                    conn.disconnect();

                    conn = (HttpURLConnection) new URL(minorUpdateUrl).openConnection();
                    conn.setUseCaches(false);
                    conn.setRequestMethod("HEAD");
                    conn.setConnectTimeout(15000);
                    conn.setReadTimeout(15000);
                    conn.connect();
                    responseCode = conn.getResponseCode();
                    if (responseCode == 200 || responseCode == 201) {
                        result = true;
                        break;
                    }
                }
            }

            conn.disconnect();
        } catch (final Exception e) {
            if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
        }

        return result;
    }

    @Override
    protected void onPostExecute(final Boolean result) {
        if (result != null && result && fetchListener != null) fetchListener.onResult(true);
    }
}