package awais.instagrabber.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

import awais.instagrabber.R;
import awais.instagrabber.asyncs.DownloadAsync;
import awais.instagrabber.asyncs.ProfilePictureFetcher;
import awais.instagrabber.asyncs.ProfilePictureFetcher.ProfilePictureFetchMode;
import awais.instagrabber.databinding.ActivityProfileBinding;
import awais.instagrabber.dialogs.ProfileSettingsDialog;
import awais.instagrabber.interfaces.FetchListener;
import awais.instagrabber.models.ProfileModel;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.Utils;

import static awais.instagrabber.utils.Constants.PROFILE_FETCH_MODE;

public final class ProfileViewer extends BaseLanguageActivity {
    private final ProfilePictureFetchMode[] fetchModes = {
            ProfilePictureFetchMode.INSTADP,
            ProfilePictureFetchMode.INSTA_STALKER,
            ProfilePictureFetchMode.INSTAFULLSIZE,
    };
    private ActivityProfileBinding profileBinding;
    private ProfileModel profileModel;
    private MenuItem menuItemDownload;
    private String profilePicUrl;
    private FragmentManager fragmentManager;
    private FetchListener<String> fetchListener;
    private boolean errorHandled = false;
    private boolean fallbackToProfile = false;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileBinding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(profileBinding.getRoot());

        setSupportActionBar(profileBinding.toolbar.toolbar);

        final Intent intent = getIntent();
        if (intent == null || !intent.hasExtra(Constants.EXTRAS_PROFILE)
                || (profileModel = (ProfileModel) intent.getSerializableExtra(Constants.EXTRAS_PROFILE)) == null) {
            Utils.errorFinish(this);
            return;
        }

        fragmentManager = getSupportFragmentManager();

        final String id = profileModel.getId();
        final String username = profileModel.getUsername();

        profileBinding.toolbar.toolbar.setTitle(username);

        profileBinding.progressView.setVisibility(View.VISIBLE);
        profileBinding.imageViewer.setVisibility(View.VISIBLE);

        profileBinding.imageViewer.setZoomable(true);
        profileBinding.imageViewer.setZoomTransitionDuration(420);
        profileBinding.imageViewer.setMaximumScale(7.2f);

        final int fetchIndex = Math.min(2, Math.max(0, Utils.settingsHelper.getInteger(PROFILE_FETCH_MODE)));
        final ProfilePictureFetchMode fetchMode = fetchModes[fetchIndex];

        fetchListener = profileUrl -> {
            profilePicUrl = profileUrl;

            if (!fallbackToProfile && Utils.isEmpty(profilePicUrl)) {
                fallbackToProfile = true;
                new ProfilePictureFetcher(username, id, fetchListener, fetchMode).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                return;
            }

            if (errorHandled && fallbackToProfile || Utils.isEmpty(profilePicUrl))
                profilePicUrl = profileModel.getHdProfilePic();

            Picasso.get().load(profilePicUrl).into(profileBinding.imageViewer, new Callback() {
                @Override
                public void onSuccess() {
                    if (menuItemDownload != null) menuItemDownload.setEnabled(true);
                    showImageInfo();
                    profileBinding.progressView.setVisibility(View.GONE);
                }

                @Override
                public void onError(final Exception e) {
                    fallbackToProfile = true;
                    if (!errorHandled) {
                        errorHandled = true;
                        new ProfilePictureFetcher(username, id, fetchListener, fetchModes[Math.min(2, Math.max(0, fetchIndex + 1))])
                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        Picasso.get().load(profileModel.getHdProfilePic()).into(profileBinding.imageViewer);
                        showImageInfo();
                    }
                    profileBinding.progressView.setVisibility(View.GONE);
                }

                private void showImageInfo() {
                    final Drawable drawable = profileBinding.imageViewer.getDrawable();
                    if (drawable != null) {
                        final StringBuilder info = new StringBuilder(getString(R.string.profile_viewer_imageinfo, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight()));
                        if (drawable instanceof BitmapDrawable) {
                            final Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            if (bitmap != null) {
                                final String colorDepthPrefix = getString(R.string.profile_viewer_colordepth_prefix);
                                switch (bitmap.getConfig()) {
                                    case ALPHA_8:
                                        info.append(colorDepthPrefix).append(" 8-bits(A)");
                                        break;
                                    case RGB_565:
                                        info.append(colorDepthPrefix).append(" 16-bits-A");
                                        break;
                                    case ARGB_4444:
                                        info.append(colorDepthPrefix).append(" 16-bits+A");
                                        break;
                                    case ARGB_8888:
                                        info.append(colorDepthPrefix).append(" 32-bits+A");
                                        break;
                                    case RGBA_F16:
                                        info.append(colorDepthPrefix).append(" 64-bits+A");
                                        break;
                                    case HARDWARE:
                                        info.append(colorDepthPrefix).append(" auto");
                                        break;
                                }
                            }
                        }
                        profileBinding.imageInfo.setText(info);
                        profileBinding.imageInfo.setVisibility(View.VISIBLE);
                    }
                }
            });
        };

        new ProfilePictureFetcher(username, id, fetchListener, fetchMode).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void downloadProfilePicture() {
        int error = 0;

        if (profileModel != null) {
            final File dir = new File(Environment.getExternalStorageDirectory(), "Download");
            if (dir.exists() || dir.mkdirs()) {

                final File saveFile = new File(dir, profileModel.getUsername() + '_' + System.currentTimeMillis()
                        + Utils.getExtensionFromModel(profilePicUrl, profileModel));

                new DownloadAsync(this,
                        profilePicUrl,
                        saveFile,
                        result -> {
                            final int toastRes = result != null && result.exists() ?
                                    R.string.downloader_downloaded_in_folder : R.string.downloader_error_download_file;
                            Toast.makeText(this, toastRes, Toast.LENGTH_SHORT).show();
                        }).setItems(null, profileModel.getUsername()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else error = 1;
        } else error = 2;

        if (error == 1) Toast.makeText(this, R.string.downloader_error_creating_folder, Toast.LENGTH_SHORT).show();
        else if (error == 2) Toast.makeText(this, R.string.downloader_unknown_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        final MenuItem.OnMenuItemClickListener menuItemClickListener = item -> {
            if (item == menuItemDownload) {
                downloadProfilePicture();
            } else {
                new ProfileSettingsDialog().show(fragmentManager, "settings");
            }
            return true;
        };

        menu.findItem(R.id.action_search).setVisible(false);
        menuItemDownload = menu.findItem(R.id.action_download);
        menuItemDownload.setVisible(true);
        menuItemDownload.setEnabled(false);
        menuItemDownload.setOnMenuItemClickListener(menuItemClickListener);

        final MenuItem menuItemSettings = menu.findItem(R.id.action_settings);
        menuItemSettings.setVisible(true);
        menuItemSettings.setOnMenuItemClickListener(menuItemClickListener);

        return true;
    }
}