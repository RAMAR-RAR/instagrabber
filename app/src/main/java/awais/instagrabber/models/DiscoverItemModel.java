package awais.instagrabber.models;

public final class DiscoverItemModel extends BasePostModel {
    private final int mediaType;
    private boolean moreAvailable;
    private String nextMaxId;

    public DiscoverItemModel(final int mediaType, final String postId, final String shortCode, final String thumbnail) {
        this.postId = postId;
        this.mediaType = mediaType;
        this.shortCode = shortCode;
        this.displayUrl = thumbnail;
    }

    public int getMediaType() {
        return mediaType;
    }

    public void setMore(final boolean moreAvailable, final String nextMaxId) {
        this.moreAvailable = moreAvailable;
        this.nextMaxId = nextMaxId;
    }

    public boolean hasMore() {
        return moreAvailable;
    }

    public String getNextMaxId() {
        return nextMaxId;
    }
}