package awais.instagrabber.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import awais.instagrabber.R;
import awais.instagrabber.adapters.viewholder.DiscoverViewHolder;
import awais.instagrabber.models.DiscoverItemModel;

public final class DiscoverAdapter extends RecyclerView.Adapter<DiscoverViewHolder> {
    private final ArrayList<DiscoverItemModel> discoverItemModels;
    private final View.OnClickListener clickListener;
    private final View.OnLongClickListener longClickListener;
    private LayoutInflater layoutInflater;
    public boolean isSelecting = false;

    public DiscoverAdapter(final ArrayList<DiscoverItemModel> discoverItemModels, final View.OnClickListener clickListener,
                           final View.OnLongClickListener longClickListener) {
        this.discoverItemModels = discoverItemModels;
        this.longClickListener = longClickListener;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public DiscoverViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        if (layoutInflater == null) layoutInflater = LayoutInflater.from(parent.getContext());
        return new DiscoverViewHolder(layoutInflater.inflate(R.layout.item_post, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final DiscoverViewHolder holder, final int position) {
        final DiscoverItemModel itemModel = discoverItemModels.get(position);
        if (itemModel != null) {
            itemModel.setPosition(position);
            holder.itemView.setTag(itemModel);

            holder.itemView.setOnClickListener(clickListener);
            holder.itemView.setOnLongClickListener(longClickListener);

            final int mediaType = itemModel.getMediaType();

            holder.typeIcon.setVisibility(mediaType == 2 || mediaType == 8 ? View.VISIBLE : View.GONE);
            holder.typeIcon.setImageResource(mediaType == 8 ? R.drawable.slider : R.drawable.video);

            holder.selectedView.setVisibility(itemModel.isSelected() ? View.VISIBLE : View.GONE);
            holder.progressView.setVisibility(View.VISIBLE);

            Picasso.get().load(itemModel.getDisplayUrl()).into(holder.postImage, new Callback() {
                @Override
                public void onSuccess() {
                    holder.progressView.setVisibility(View.GONE);
                }

                @Override
                public void onError(final Exception e) {
                    holder.progressView.setVisibility(View.GONE);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return discoverItemModels == null ? 0 : discoverItemModels.size();
    }
}