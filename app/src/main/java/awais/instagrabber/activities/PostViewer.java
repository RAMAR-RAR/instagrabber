package awais.instagrabber.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import awais.instagrabber.R;
import awais.instagrabber.adapters.PostsMediaAdapter;
import awais.instagrabber.asyncs.PostFetcher;
import awais.instagrabber.asyncs.ProfileFetcher;
import awais.instagrabber.customviews.CommentMentionClickSpan;
import awais.instagrabber.customviews.helpers.SwipeGestureListener;
import awais.instagrabber.databinding.ActivityViewerBinding;
import awais.instagrabber.interfaces.ItemGetter;
import awais.instagrabber.interfaces.SwipeEvent;
import awais.instagrabber.models.BasePostModel;
import awais.instagrabber.models.PostModel;
import awais.instagrabber.models.ProfileModel;
import awais.instagrabber.models.ViewerPostModel;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.DownloadMethod;
import awais.instagrabber.utils.Utils;

public final class PostViewer extends BaseLanguageActivity {
    private ActivityViewerBinding viewerBinding;
    private String url, prevUsername, commentsEndCursor;
    private ProfileModel profileModel;
    private BasePostModel postModel;
    private ViewerPostModel viewerPostModel;
    private SimpleExoPlayer player;
    private ArrayAdapter<String> profileDialogAdapter;
    private View  viewsContainer, viewerCaptionParent;
    private GestureDetectorCompat gestureDetector;
    private SwipeEvent swipeEvent;
    private CharSequence postCaption = null, postShortCode;
    private Resources resources;
    private boolean session = false, isFromShare;
    private int slidePos = 0, lastSlidePos = 0;
    private ItemGetter.ItemGetType itemGetType;
    @SuppressLint("ClickableViewAccessibility")
    final View.OnTouchListener gestureTouchListener = new View.OnTouchListener() {
        private float startX;
        private float startY;

        @Override
        public boolean onTouch(final View v, final MotionEvent event) {
            if (v == viewerCaptionParent) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startX = event.getX();
                        startY = event.getY();
                        break;

                    case MotionEvent.ACTION_UP:
                        if (!(Utils.isEmpty(postCaption) ||
                                Math.abs(startX - event.getX()) > 50 || Math.abs(startY - event.getY()) > 50)) {
                            Utils.copyText(PostViewer.this, postCaption);
                            return false;
                        }
                }
            }
            return gestureDetector.onTouchEvent(event);
        }
    };
    private final DialogInterface.OnClickListener profileDialogListener = (dialog, which) -> {
        final String username = viewerPostModel.getUsername();

        if (which == 0) {
            searchUsername(username);
        } else if (profileModel != null && which == 1) {
            startActivity(new Intent(this, ProfileViewer.class)
                    .putExtra(Constants.EXTRAS_PROFILE, profileModel));
        }
    };
    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            if (v == viewerBinding.topPanel.ivProfilePic) {
                new AlertDialog.Builder(PostViewer.this).setAdapter(profileDialogAdapter, profileDialogListener)
                        .setNeutralButton(R.string.cancel, null).setTitle(viewerPostModel.getUsername()).show();

            } else if (v == viewerBinding.bottomPanel.btnMute) {
                if (player != null) {
                    final float intVol = player.getVolume() == 0f ? 1f : 0f;
                    player.setVolume(intVol);
                    viewerBinding.bottomPanel.btnMute.setImageResource(intVol == 0f ? R.drawable.vol : R.drawable.mute);
                    Utils.sessionVolumeFull = intVol == 1f;
                }

            } else {
                final Object tag = v.getTag();
                if (tag instanceof ViewerPostModel) {
                    viewerPostModel = (ViewerPostModel) tag;
                    slidePos = Math.max(0, viewerPostModel.getPosition());
                    refreshPost();
                }
            }
        }
    };
    private final PostsMediaAdapter mediaAdapter = new PostsMediaAdapter(null, onClickListener);

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewerBinding = ActivityViewerBinding.inflate(getLayoutInflater());
        setContentView(viewerBinding.getRoot());

        final Intent intent = getIntent();
        if (intent == null || !intent.hasExtra(Constants.EXTRAS_POST)
                || (postModel = (PostModel) intent.getSerializableExtra(Constants.EXTRAS_POST)) == null) {
            Utils.errorFinish(this);
            return;
        }

        if (intent.hasExtra(Constants.EXTRAS_TYPE))
            itemGetType = (ItemGetter.ItemGetType) intent.getSerializableExtra(Constants.EXTRAS_TYPE);

        resources = getResources();

        final View viewStoryPost = findViewById(R.id.viewStoryPost);
        if (viewStoryPost != null) viewStoryPost.setVisibility(View.GONE);

        viewerBinding.topPanel.title.setMovementMethod(new LinkMovementMethod());
        viewerBinding.topPanel.title.setMentionClickListener((view, text, isHashtag) ->
                onClickListener.onClick(viewerBinding.topPanel.ivProfilePic));
        viewerBinding.topPanel.ivProfilePic.setOnClickListener(onClickListener);

        profileDialogAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                new String[]{resources.getString(R.string.open_profile), resources.getString(R.string.view_pfp)});

        postModel.setPosition(intent.getIntExtra(Constants.EXTRAS_INDEX, -1));
        postShortCode = postModel.getShortCode();

        final boolean postIdNull = postModel.getPostId() == null;
        if (!postIdNull)
            setupPostInfoBar(intent.getStringExtra(Constants.EXTRAS_USER), postModel.isVideo());

        isFromShare = postModel.getPosition() == -1 || postIdNull;

        viewerCaptionParent = (View) viewerBinding.bottomPanel.viewerCaption.getParent();
        viewsContainer = (View) viewerBinding.bottomPanel.tvVideoViews.getParent();

        viewerBinding.mediaList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        viewerBinding.mediaList.setAdapter(mediaAdapter);
        viewerBinding.mediaList.setVisibility(View.GONE);

        swipeEvent = isRight -> {
            final List<? extends BasePostModel> itemGetterItems;
            final boolean isMainSwipe;

            if (itemGetType != null && Main.itemGetter != null) {
                itemGetterItems = Main.itemGetter.get(itemGetType);
                isMainSwipe = !(itemGetterItems.size() < 1 || itemGetType == ItemGetter.ItemGetType.MAIN_ITEMS && isFromShare);
            } else {
                itemGetterItems = null;
                isMainSwipe = false;
            }

            final BasePostModel[] basePostModels = mediaAdapter != null ? mediaAdapter.getPostModels() : null;
            final int slides = basePostModels != null ? basePostModels.length : 0;

            int position = postModel.getPosition();

            if (isRight) {
                --slidePos;
                if (!isMainSwipe && slidePos < 0) slidePos = 0;
                if (slides > 0 && slidePos >= 0) {
                    if (basePostModels[slidePos] instanceof ViewerPostModel) {
                        viewerPostModel = (ViewerPostModel) basePostModels[slidePos];
                    }
                    refreshPost();
                    return;
                }
                if (isMainSwipe && --position < 0) position = itemGetterItems.size() - 1;
            } else {
                ++slidePos;
                if (!isMainSwipe && slidePos >= slides) slidePos = slides - 1;
                if (slides > 0 && slidePos < slides) {
                    if (basePostModels[slidePos] instanceof ViewerPostModel) {
                        viewerPostModel = (ViewerPostModel) basePostModels[slidePos];
                    }
                    refreshPost();
                    return;
                }
                if (isMainSwipe && ++position >= itemGetterItems.size()) position = 0;
            }

            if (isMainSwipe) {
                slidePos = 0;
                Log.d("AWAISKING_APP", "swipe left <<< post[" + position + "]: " + postModel + " -- " + slides);
                postModel = itemGetterItems.get(position);
                postModel.setPosition(position);
                viewPost();
            }
        };
        gestureDetector = new GestureDetectorCompat(this, new SwipeGestureListener(swipeEvent));

        viewPost();
    }

    private void viewPost() {
        lastSlidePos = 0;
        mediaAdapter.setData(null);
        viewsContainer.setVisibility(View.GONE);
        viewerCaptionParent.setVisibility(View.GONE);
        viewerBinding.mediaList.setVisibility(View.GONE);
        viewerBinding.bottomPanel.btnMute.setVisibility(View.GONE);
        viewerBinding.bottomPanel.tvPostDate.setVisibility(View.GONE);
        viewerBinding.bottomPanel.btnComments.setVisibility(View.GONE);
        viewerBinding.bottomPanel.btnDownload.setVisibility(View.INVISIBLE);
        viewerBinding.bottomPanel.viewerCaption.setText(null);
        viewerBinding.bottomPanel.viewerCaption.setMentionClickListener(null);

        new PostFetcher(postModel.getShortCode(), result -> {
            if (result == null || result.length < 1) {
                Toast.makeText(this, R.string.downloader_unknown_error, Toast.LENGTH_SHORT).show();
                return;
            }

            viewerPostModel = result[0];
            commentsEndCursor = viewerPostModel.getCommentsEndCursor();

            mediaAdapter.setData(result);
            if (result.length > 1) {
                viewerBinding.mediaList.setVisibility(View.VISIBLE);
            }

            viewerCaptionParent.setOnTouchListener(gestureTouchListener);
            viewerBinding.playerView.setOnTouchListener(gestureTouchListener);
            viewerBinding.imageViewer.setOnSingleFlingListener((e1, e2, velocityX, velocityY) -> {
                final float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(e2.getY() - e1.getY()) && Math.abs(diffX) > SwipeGestureListener.SWIPE_THRESHOLD
                        && Math.abs(velocityX) > SwipeGestureListener.SWIPE_VELOCITY_THRESHOLD) {
                    swipeEvent.onSwipe(diffX > 0);
                    return true;
                }
                return false;
            });

            final long commentsCount = viewerPostModel.getCommentsCount();
            viewerBinding.bottomPanel.commentsCount.setText(String.valueOf(commentsCount));
            viewerBinding.bottomPanel.btnComments.setVisibility(View.VISIBLE);

            if (commentsCount > 0) {
                viewerBinding.bottomPanel.btnComments.setOnClickListener(v ->
                        startActivityForResult(new Intent(this, CommentsViewer.class)
                                .putExtra(Constants.EXTRAS_END_CURSOR, commentsEndCursor)
                                .putExtra(Constants.EXTRAS_SHORTCODE, postShortCode), 6969));
                viewerBinding.bottomPanel.btnComments.setClickable(true);
                viewerBinding.bottomPanel.btnComments.setEnabled(true);
            } else {
                viewerBinding.bottomPanel.btnComments.setOnClickListener(null);
                viewerBinding.bottomPanel.btnComments.setClickable(false);
                viewerBinding.bottomPanel.btnComments.setEnabled(false);
            }

            if (postModel instanceof PostModel) {
                final PostModel postModel = (PostModel) this.postModel;
                postModel.setPostId(viewerPostModel.getPostId());
                postModel.setTimestamp(viewerPostModel.getTimestamp());
                postModel.setPostCaption(viewerPostModel.getPostCaption());
            }

            setupPostInfoBar(viewerPostModel.getUsername(), viewerPostModel.isVideo());

            postCaption = postModel.getPostCaption();
            viewerCaptionParent.setVisibility(View.VISIBLE);

            if (Utils.hasMentions(postCaption)) {
                viewerBinding.bottomPanel.viewerCaption.setText(Utils.getMentionText(postCaption), TextView.BufferType.SPANNABLE);
                viewerBinding.bottomPanel.viewerCaption.setMentionClickListener((view, text, isHashtag) ->
                        new AlertDialog.Builder(PostViewer.this).setTitle(text)
                                .setMessage(isHashtag ? R.string.comment_view_mention_hash_search : R.string.comment_view_mention_user_search)
                                .setNegativeButton(R.string.cancel, null).setPositiveButton(R.string.ok,
                                (dialog, which) -> searchUsername(text)).show());
            } else {
                viewerBinding.bottomPanel.viewerCaption.setMentionClickListener(null);
                viewerBinding.bottomPanel.viewerCaption.setText(postCaption);
            }

            viewerBinding.bottomPanel.btnDownload.setOnClickListener(v -> {
                if (ContextCompat.checkSelfPermission(this, Utils.PERMS[0]) == PackageManager.PERMISSION_GRANTED)
                    showDownloadDialog();
                else
                    ActivityCompat.requestPermissions(this, Utils.PERMS, 8020);
            });

            refreshPost();
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void searchUsername(final String text) {
        if (Main.scanHack != null) {
            Main.scanHack.onResult(text);
            finish();
        }
    }

    private void setupVideo() {
        viewerBinding.playerView.setVisibility(View.VISIBLE);
        viewerBinding.bottomPanel.btnDownload.setVisibility(View.VISIBLE);
        viewerBinding.bottomPanel.btnMute.setVisibility(View.VISIBLE);
        viewsContainer.setVisibility(View.VISIBLE);
        viewerBinding.progressView.setVisibility(View.GONE);
        viewerBinding.imageViewer.setVisibility(View.GONE);
        viewerBinding.imageViewer.setImageDrawable(null);

        viewerBinding.bottomPanel.tvVideoViews.setText(String.valueOf(viewerPostModel.getVideoViews()));

        player = new SimpleExoPlayer.Builder(this).build();
        viewerBinding.playerView.setPlayer(player);
        float vol = Utils.settingsHelper.getBoolean(Constants.MUTED_VIDEOS) ? 0f : 1f;
        if (vol == 0f && Utils.sessionVolumeFull) vol = 1f;

        player.setVolume(vol);
        player.setPlayWhenReady(Utils.settingsHelper.getBoolean(Constants.AUTOPLAY_VIDEOS));
        final ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultDataSourceFactory(this, "instagram"))
                .createMediaSource(Uri.parse(url));
        mediaSource.addEventListener(new Handler(), new MediaSourceEventListener() {
            @Override
            public void onLoadCompleted(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                viewerBinding.progressView.setVisibility(View.GONE);
            }

            @Override
            public void onLoadStarted(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                viewerBinding.progressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadCanceled(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                viewerBinding.progressView.setVisibility(View.GONE);
            }

            @Override
            public void onLoadError(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData, final IOException error, final boolean wasCanceled) {
                viewerBinding.progressView.setVisibility(View.GONE);
            }
        });
        player.prepare(mediaSource);

        player.setVolume(vol);
        viewerBinding.bottomPanel.btnMute.setImageResource(vol == 0f ? R.drawable.vol : R.drawable.mute);

        viewerBinding.bottomPanel.btnMute.setOnClickListener(onClickListener);
    }

    private void setupImage() {
        viewsContainer.setVisibility(View.GONE);
        viewerBinding.playerView.setVisibility(View.GONE);
        viewerBinding.progressView.setVisibility(View.VISIBLE);
        viewerBinding.bottomPanel.btnMute.setVisibility(View.GONE);
        viewerBinding.bottomPanel.btnDownload.setVisibility(View.VISIBLE);

        viewerBinding.imageViewer.setImageDrawable(null);
        viewerBinding.imageViewer.setVisibility(View.VISIBLE);
        viewerBinding.imageViewer.setZoomable(true);
        viewerBinding.imageViewer.setZoomTransitionDuration(420);
        viewerBinding.imageViewer.setMaximumScale(7.2f);

        Picasso.get().load(url).into(viewerBinding.imageViewer, new Callback() {
            @Override
            public void onSuccess() {
                viewerBinding.progressView.setVisibility(View.GONE);
            }

            @Override
            public void onError(final Exception e) {
                viewerBinding.progressView.setVisibility(View.GONE);
            }
        });
    }

    private void showDownloadDialog() {
        final ArrayList<BasePostModel> postModels = new ArrayList<>();

        if (!session && viewerBinding.mediaList.getVisibility() == View.VISIBLE) {
            final DialogInterface.OnClickListener clickListener = (dialog, which) -> {
                postModels.clear();

                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    final BasePostModel[] adapterPostModels = mediaAdapter.getPostModels();
                    for (int i = 0, size = mediaAdapter.getItemCount(); i < size; ++i) {
                        if (adapterPostModels[i] instanceof ViewerPostModel)
                            postModels.add(adapterPostModels[i]);
                    }
                } else if (which == DialogInterface.BUTTON_POSITIVE) {
                    postModels.add(viewerPostModel);
                } else {
                    session = true;
                    postModels.add(viewerPostModel);
                }

                if (postModels.size() > 0)
                    Utils.batchDownload(this, viewerPostModel.getUsername(), DownloadMethod.DOWNLOAD_POST_VIEWER, postModels);
            };

            new AlertDialog.Builder(this).setTitle(R.string.post_viewer_download_dialog_title)
                    .setMessage(R.string.post_viewer_download_message)
                    .setNeutralButton(R.string.post_viewer_download_session, clickListener).setPositiveButton(R.string.post_viewer_download_current, clickListener)
                    .setNegativeButton(R.string.post_viewer_download_album, clickListener).show();
        } else {
            Utils.batchDownload(this, viewerPostModel.getUsername(), DownloadMethod.DOWNLOAD_POST_VIEWER, Collections.singletonList(viewerPostModel));
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 8020 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            showDownloadDialog();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 6969) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT < 24) releasePlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT >= 24) releasePlayer();
    }

    private void refreshPost() {
        postShortCode = postModel.getShortCode();
        if (viewerBinding.mediaList.getVisibility() == View.VISIBLE) {
            ViewerPostModel item = mediaAdapter.getItemAt(lastSlidePos);
            if (item != null) {
                item.setCurrentSlide(false);
                mediaAdapter.notifyItemChanged(lastSlidePos, item);
            }

            item = mediaAdapter.getItemAt(slidePos);
            if (item != null) {
                item.setCurrentSlide(true);
                mediaAdapter.notifyItemChanged(slidePos, item);
            }
        }
        lastSlidePos = slidePos;

        setupPostInfoBar(viewerPostModel.getUsername(), viewerPostModel.isVideo());

        if (postModel instanceof PostModel) {
            final PostModel postModel = (PostModel) this.postModel;
            postModel.setPostId(viewerPostModel.getPostId());
            postModel.setTimestamp(viewerPostModel.getTimestamp());
            postModel.setPostCaption(viewerPostModel.getPostCaption());
        }

        viewerBinding.bottomPanel.tvPostDate.setText(viewerPostModel.getPostDate());
        viewerBinding.bottomPanel.tvPostDate.setVisibility(View.VISIBLE);
        viewerBinding.bottomPanel.tvPostDate.setSelected(true);

        url = viewerPostModel.getDisplayUrl();
        releasePlayer();
        if (viewerPostModel.isVideo()) setupVideo();
        else setupImage();
    }

    private void releasePlayer() {
        if (player == null) return;
        player.release();
        player = null;
    }

    private void setupPostInfoBar(final String from, final boolean isVideo) {
        if (prevUsername == null || !prevUsername.equals(from)) {
            viewerBinding.topPanel.ivProfilePic.setImageBitmap(null);
            viewerBinding.topPanel.ivProfilePic.setImageDrawable(null);
            viewerBinding.topPanel.ivProfilePic.setImageResource(0);

            if (from.charAt(0) != '#')
                new ProfileFetcher(from, result -> {
                    profileModel = result;

                    if (result != null) {
                        final String hdProfilePic = result.getHdProfilePic();
                        final String sdProfilePic = result.getSdProfilePic();

                        final boolean hdPicEmpty = Utils.isEmpty(hdProfilePic);
                        Picasso.get().load(hdPicEmpty ? sdProfilePic : hdProfilePic).into(viewerBinding.topPanel.ivProfilePic, new Callback() {
                            private boolean loaded = true;

                            @Override
                            public void onSuccess() {
                                viewerBinding.topPanel.ivProfilePic.setEnabled(true);
                                viewerBinding.topPanel.ivProfilePic.setOnClickListener(onClickListener);
                            }

                            @Override
                            public void onError(final Exception e) {
                                viewerBinding.topPanel.ivProfilePic.setEnabled(false);
                                viewerBinding.topPanel.ivProfilePic.setOnClickListener(null);
                                if (loaded) {
                                    loaded = false;
                                    if (!Utils.isEmpty(sdProfilePic))
                                        Picasso.get().load(sdProfilePic).into(viewerBinding.topPanel.ivProfilePic, this);
                                }
                            }
                        });
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            prevUsername = from;
        }

        final String titlePrefix = resources.getString(isVideo ? R.string.post_viewer_video_post : R.string.post_viewer_image_post);
        if (Utils.isEmpty(from)) viewerBinding.topPanel.title.setText(titlePrefix);
        else {
            final CharSequence titleText = resources.getString(R.string.post_viewer_post_from, titlePrefix, from) + " ";
            final int titleLen = titleText.length();
            final SpannableString spannableString = new SpannableString(titleText);
            spannableString.setSpan(new CommentMentionClickSpan(), titleLen - from.length() - 1, titleLen - 1, 0);
            viewerBinding.topPanel.title.setText(spannableString);
        }
    }
}